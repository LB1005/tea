/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.attach.admin.persists.data.converter;

import com.tarena.passport.protocol.LoginUser;
import com.tarena.passport.sdk.context.SecurityContext;
import com.tarena.tp.attach.admin.dto.AttachDTO;
import com.tarena.tp.attach.admin.param.AttachParam;
import com.tarena.tp.attach.po.Attach;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AttachConverter {

    public Attach convertParamToModel(AttachParam attachParam) {
        if (attachParam == null) {
            return null;
        }
        Attach attach = new Attach();
        BeanUtils.copyProperties(attachParam,attach);
        attach.setFileUuid(String.valueOf(System.currentTimeMillis()));
        attach.setDownloadTimes(0);
        attach.setRemark("");
        attach.setIsCover(0);
        attach.setBusinessId(0);
        attach.setBusinessType(0);
        attach.setStatus(0);
        LoginUser loginUser = SecurityContext.getLoginToken();
        if (loginUser != null) {
            attach.setCreateUserId(loginUser.getUserId());
            attach.setCreateUserName(loginUser.getUserName());
            attach.setGmtCreate(System.currentTimeMillis());
            attach.setModifiedUserId(loginUser.getUserId());
            attach.setModifiedUserName(loginUser.getUserName());
            attach.setGmtModified(System.currentTimeMillis());
        }
        return attach;
    }

    public AttachDTO convertModelToDto(Attach source) {
        if (source == null) {
            return null;
        }
        AttachDTO object = new AttachDTO();
        BeanUtils.copyProperties(source,object);
        return object;
    }

    public List<AttachDTO> assembleModelListToDtoList(List<Attach> sources) {
        if (sources == null) {
            return null;
        }
        List<AttachDTO> objects = new ArrayList<>();
        for (Attach source : sources) {
            AttachDTO object = new AttachDTO();
            BeanUtils.copyProperties(source,object);
            objects.add(object);
        }
        return objects;
    }
}
