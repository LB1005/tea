/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.attach.server.param;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class AttachUpdateParam implements Serializable {

    @NotNull(message = "是否是封面不能为空[1:是;0:否;]")
    @ApiModelProperty("是否封面")
    private Integer isCover;

    @NotNull(message = "系统类型")
    @ApiModelProperty("系统类型")
    private Integer businessType;

    @NotNull(message = "业务id不能为空")
    @ApiModelProperty("业务id")
    private Integer businessId;

    @NotNull(message = "id不能为空")
    @ApiModelProperty
    private Integer id;
}
