/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tarena.tp.attach.server.persists.repository;

import com.tarena.tp.attach.common.FileStatusEnum;
import com.tarena.tp.attach.common.util.FileUtil;
import com.tarena.tp.attach.po.Attach;
import com.tarena.tp.attach.server.dao.AttachMapper;
import com.tarena.tp.attach.server.dto.AttachDTO;
import com.tarena.tp.attach.server.param.AttachParam;
import com.tarena.tp.attach.server.param.AttachUpdateParam;
import com.tarena.tp.attach.server.persists.data.converter.AttachConverter;
import com.tarena.tp.attach.server.query.AttachQuery;
import java.util.List;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AttachRepositoryImpl implements AttachRepository {

    @Value("${image_path}")
    private String imagePath;

    @Resource
    private AttachMapper attachMapper;

    @Resource
    private AttachConverter attachConverter;

    @Override
    public AttachDTO insertAttachInfo(AttachParam attachParam) {
        Attach attach = attachConverter.convertParamToModel(attachParam);
        attachMapper.insert(attach);
        return attachConverter.convertModelToDto(attach);
    }

    @Override
    public List<AttachDTO> getAttachInfo(AttachQuery attachQuery) {
        List<Attach> attachList = attachMapper.getAttachInfo(attachQuery);
        return attachConverter.assembleModelListToDtoList(attachList);
    }

    @Override
    public int batchUpdateAttachByIdList(List<AttachUpdateParam> attachUpdateParamList) {
        return attachMapper.batchUpdateAttachByIdList(attachUpdateParamList);
    }

    @Override
    public int deleteAttachByBusinessIdAndBusinessType(AttachUpdateParam attachUpdateParam) {
        return attachMapper.deleteAttachByBusinessIdAndBusinessType(attachUpdateParam);
    }

    @Override
    public int deleteAttachById(Long id) {
        return attachMapper.deleteAttachById(id);
    }
    @Override
    public int deleteAttachInfoByParam(AttachQuery attachQuery) {
        List<Attach> attachList = attachMapper.getAttachInfo(attachQuery);
        List<Integer> idList = attachList.stream().map(e -> e.getId()).collect(Collectors.toList());
        List<String> filePathList = attachList.stream().map(e -> imagePath + e.getFileUuid()).collect(Collectors.toList());
        FileUtil.deleteFile(filePathList);
        return attachMapper.batchUpdateAttachStatus(FileStatusEnum.UNENABLE.getType(),idList);
    }
}
